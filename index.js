var Service, Characteristic, VolumeCharacteristic;

const request = require('request');
const wol = require('wake_on_lan');
var xmlstart = '<?xml version="1.0"?><s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"><s:Body><u:X_SendIRCC xmlns:u="urn:schemas-sony-com:service:IRCC:1"><IRCCCode>';
var xmlend = '</IRCCCode></u:X_SendIRCC></s:Body></s:Envelope>';
var xmlcode = "";

module.exports = function(homebridge) {
  Service = homebridge.hap.Service;
  Characteristic = homebridge.hap.Characteristic;
  
  homebridge.registerAccessory("homebridge-tvservice-bravia", "tvservice-bravia", BraviaAccessory);
}

const volumeMap = {
  0: "VolumeUp",
  1: "VolumeDown"
};

function BraviaAccessory(log, config) {
  this.log = log;
  this.name = config["name"];
  this.host = config["ip"];
  this.port = config["port"];
  this.psk = config["psk"];
  this.macaddress = config["mac"] || false;
  
  var that = this;
  this.services = [];
  this.service = new Service.Television();

  this.service.setCharacteristic(Characteristic.ConfiguredName, this.name);
  this.services.push(this.service);
  
  this.inputService = new Service.InputSource(this.name);
  
  this.speakerService = new Service.TelevisionSpeaker(
      this.name + " Volume",
      "volumeService"
    );

    this.speakerService
      .setCharacteristic(Characteristic.Active, Characteristic.Active.ACTIVE)
      .setCharacteristic(
        Characteristic.VolumeControlType,
        Characteristic.VolumeControlType.ABSOLUTE
      );

    this.speakerService
      .getCharacteristic(Characteristic.VolumeSelector)
      .on("set", function(code, callback) {
          
          if(code == 0) {
              xmlcode = "AAAAAQAAAAEAAAASAw=="
          }
          else xmlcode = "AAAAAQAAAAEAAAATAw=="
          
          request({
              method: 'POST',
              uri: 'http://'+that.host+":"+that.port+'/sony/IRCC',
              headers: {
                  'X-Auth-PSK': that.psk,
                  'Content-Type': 'text/xml; charset=UTF-8',
                  'SOAPACTION': 'urn:schemas-sony-com:service:IRCC:1#X_SendIRCC'
              },
              body: xmlstart+xmlcode+xmlend
          },
          function (error, response, body) {
              if (error) {
                  return this.log.debug('request failed:'+error);
              }
              
              else callback(false);
          });
          
      });
  
  this.services.push(this.speakerService);
  
  this.defaultInputs = [{
              code: 'AAAAAgAAABoAAABaAw==',
              name: 'Sonoc Arc',
              type: Characteristic.InputSourceType.HDMI
          },
          {
              code: 'AAAAAgAAABoAAABbAw==',
              name: 'HDMI Switch',
              type: Characteristic.InputSourceType.HDMI
          },
          {
              code: 'AAAAAQAAAAEAAABAAw==',
              name: 'Video 1',
              type: Characteristic.InputSourceType.Playback
          },
         {
              code: 'AAAAAQAAAAEAAABBAw==',
              name: 'Video 2',
              type: Characteristic.InputSourceType.Playback
          },
          {
              code: 'AAAAAQAAAAEAAAAkAw==',
              name: 'Live TV',
              type: Characteristic.InputSourceType.TV
          }
      ];
      
      this.inputAppIds = new Array();
          // predefined inputs
           this.defaultInputs.forEach((value, i) => {

              let tmpDefaultSource = new Service.InputSource(value.name, 'inputSource' + i);
              tmpDefaultSource
                  .setCharacteristic(Characteristic.Identifier, i)
                  .setCharacteristic(Characteristic.ConfiguredName, value.name)
                  .setCharacteristic(Characteristic.IsConfigured, Characteristic.IsConfigured.CONFIGURED)
                  .setCharacteristic(Characteristic.InputSourceType, value.type)
                  .setCharacteristic(Characteristic.CurrentVisibilityState, Characteristic.CurrentVisibilityState.SHOWN);

              this.service.addLinkedService(tmpDefaultSource);
              this.services.push(tmpDefaultSource);
              this.inputAppIds.push(value.code);

          });
          
  this.service
          .getCharacteristic(Characteristic.ActiveIdentifier)
          .on('set', (code, callback) => {
              this.log('bravia - input source changed, new input source identifier: %d, source appId: %s', code, this.inputAppIds[code]);
              xmlcode = this.inputAppIds[code];

              request({
                  method: 'POST',
                  uri: 'http://'+that.host+":"+that.port+'/sony/IRCC',
                  headers: {
                      'X-Auth-PSK': that.psk,
                      'Content-Type': 'text/xml; charset=UTF-8',
                      'SOAPACTION': 'urn:schemas-sony-com:service:IRCC:1#X_SendIRCC'
                  },
                  body: xmlstart+xmlcode+xmlend
              },
              function (error, response, body) {
                  if (error) {
                      return this.log.debug('request failed:'+error);
                  }
              });
              callback();
          })
          .on('get', (callback) => {
              data = {"id":2,"method":"getPlayingContentInfo","version":"1.0","params":[]};

              request({
                  method: 'POST',
                  uri: 'http://'+that.host+":"+that.port+'/sony/avContent',
                  headers: {
                      'X-Auth-PSK': that.psk,
                  },
                  json: data
              },
              function (error, response, body) {
                  if (error) {
                      callback(null);
                      return;
                  }
                  if(body.result && body.result[0]["source"] == "extInput:hdmi") {
                      input = body.result[0]["uri"].split('?port=')[1]-1;
                      callback(null, input);
                  }
                  else {
                      callback(null);
                  }
              });
          });

  this.service.getCharacteristic(Characteristic.Active)
  .on('set', function(newValue, callback) {
      if(newValue == 0) {
          xmlcode = "AAAAAQAAAAEAAAAvAw==";

          request({
              method: 'POST',
              uri: 'http://'+that.host+":"+that.port+'/sony/IRCC',
              headers: {
                  'X-Auth-PSK': that.psk,
                  'Content-Type': 'text/xml; charset=UTF-8',
                  'SOAPACTION': 'urn:schemas-sony-com:service:IRCC:1#X_SendIRCC'
              },
              body: xmlstart+xmlcode+xmlend
          },
          function (error, response, body) {
              if (error) {
                  return this.log.debug('request failed:'+error);
              }
          });
      }
      else if(newValue == 1) {
          if(that.macaddress) {
              address = that.host.substring(0, that.host.lastIndexOf('.'))
              wol.wake(that.macaddress, { address: address+".255" });
          }
          else {
              xmlcode = "AAAAAQAAAAEAAAAVAw==";

              request({
                  method: 'POST',
                  uri: 'http://'+that.host+":"+that.port+'/sony/IRCC',
                  headers: {
                      'X-Auth-PSK': that.psk,
                      'Content-Type': 'text/xml; charset=UTF-8',
                      'SOAPACTION': 'urn:schemas-sony-com:service:IRCC:1#X_SendIRCC'
                  },
                  body: xmlstart+xmlcode+xmlend
              },
              function (error, response, body) {
                  if (error) {
                      return this.log.debug('request failed:'+error);
                  }
              });
          }
      }
    callback(null);
  })
  .on('get', function(callback) {
      //var session = ping.createSession (pingoptions);
      //session.pingHost (that.host, function (error, target) {
          request({
              method: 'POST',
              uri: 'http://'+that.host+":"+that.port+'/sony/IRCC',
              headers: {
                  'X-Auth-PSK': that.psk,
                  'Content-Type': 'text/xml; charset=UTF-8',
                  'SOAPACTION': 'urn:schemas-sony-com:service:IRCC:1#X_SendIRCC'
              },
              body: "pingtest"
          },
          function (error, response, body) {
              if (error) {
                  callback(null, 0);
                  return console.error('TV not reachable', error);
              }
              else {
                  data = {"id":2,"method":"getPowerStatus","version":"1.0","params":[]};

                  request({
                      method: 'POST',
                      uri: 'http://'+that.host+":"+that.port+'/sony/system',
                      headers: {
                          'X-Auth-PSK': that.psk,
                      },
                      json: data
                  },
                  function (error, response, body) {
                      if (error) {
                          callback(null, 0);
                      }
                      else {
                          if(body.result[0]['status'] == "standby") {
                              that.log.debug("TVService-Bravia: Standby");
                              callback(null, 0);
                          }
                          else {
                              that.log.debug("TVService-Bravia: Active");
                              callback(null, 1);
                          }
                      }
                      //console.log(JSON.stringify(response)+JSON.stringify(body));
                  });
                  
              }
          });
  });

  this.service.getCharacteristic(Characteristic.RemoteKey)
  .on('set', function(newValue, callback) {

      var json;
      
    switch (newValue) {
      case 4:
          code="AAAAAQAAAAEAAAB0Aw==";
          break;
      case 5:
          code="AAAAAQAAAAEAAAB1Aw==";
          break;
      case 6:
          code="AAAAAQAAAAEAAAA0Aw==";
          break;
      case 7:
          code="AAAAAQAAAAEAAAAzAw==";
          break;
      case 8:
          code="AAAAAQAAAAEAAABlAw==";
          break;
      case 9:
          code="AAAAAgAAAJcAAAAjAw==";
          break;
      case 15:
          code="AAAAAQAAAAEAAABgAw==";
          break;
      case 10:
          code="AAAAAgAAAJcAAAAYAw==";
          break;
      case 11:
          code="AAAAAgAAAJcAAAAaAw==";
          break;
    }
    
    xmlcode = code;

    request({
        method: 'POST',
        uri: 'http://'+that.host+":"+that.port+'/sony/IRCC',
        headers: {
            'X-Auth-PSK': that.psk,
            'Content-Type': 'text/xml; charset=UTF-8',
            'SOAPACTION': 'urn:schemas-sony-com:service:IRCC:1#X_SendIRCC'
        },
        body: xmlstart+xmlcode+xmlend
    },
    function (error, response, body) {
        if (error) {
            return this.log.debug('request failed:'+error);
        }
    });

    callback('null');
  });
  
}

BraviaAccessory.prototype.getServices = function() {
  return this.services;
}
